package com.example.appipare;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.CalendarView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.razerdp.widget.animatedpieview.AnimatedPieView;
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig;
import com.razerdp.widget.animatedpieview.data.SimplePieInfo;

public class MainActivity extends AppCompatActivity {
    CalendarView calendar;
    TextView textview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        dravpie();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        calendar=(CalendarView) findViewById(R.id.cwcalendar);
        textview=(TextView) findViewById(R.id.txttextview);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                String date =(month+1)+"/"+dayOfMonth+"/"+year;
                textview.setText(date);
            }
        });
    }
    public void dravpie()
    {
        AnimatedPieView mAnimatedPieView = findViewById(R.id.animatedPieView);
        AnimatedPieViewConfig config = new AnimatedPieViewConfig();
        config.startAngle(-90)// Starting angle offset
                .addData(new SimplePieInfo(30, Color.parseColor("#ff0000"), "Title1 "))//Data (bean that implements the IPieInfo interface)
                .addData(new SimplePieInfo(18.0f, Color.parseColor("#00ff00"), "title2")).drawText(true)

      .duration(2000);// draw pie animation duration

// The following two sentences can be replace directly 'mAnimatedPieView.start (config); '
        mAnimatedPieView.applyConfig(config);
        mAnimatedPieView.start();
    }




}
